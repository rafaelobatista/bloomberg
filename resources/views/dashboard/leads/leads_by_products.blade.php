<x-app-layout>
    <x-slot name="header">
        <div class="d-flex align-items-center justify-content-between">
            <span class="d-flex">
                <a href="/dashboard/pedidos">
                    <img src="{{asset('images/arrow-back.png')}}" class="w-50">
                </a>
                <h2 class="font-semibold text-xl text-gray-800 leading-tight mb-0" id="new-page-title-h3">
                    Pedidos - {{$leads[0]->product->title}}
                </h2>
            </span>
            @if($leads->count() > 0)
                <a href="/dashboard/pedidos/spreadsheet/{{$leads[0]->product_id}}">Exportar</a>
            @endif
        </div>
    </x-slot>

    <div class="pagina container">
        <div class="row mt-5 index-section">
            <div class="d-flex flex-column flex-md-row w-100">
                <div class="col-12 col-md-12">
                    <div class="list-stack">
                        <ul>
                            <li class="d-flex align-items-center justify-content-between">
                                <div class="p-0 col-md-2 col-6">
                                    Nome
                                </div>
                                <div class="p-0 col-md-4 col-6 d-md-block d-none align-items-center">
                                    Sobrenome
                                </div>
                                <div class="p-0 col-md-4 col-6 d-md-block align-items-center">
                                    Empresa
                                </div>
                                <div class="p-0 col-md-1 col-6 d-md-block d-none align-items-center">
                                    Status
                                </div>
                            </li>
                            @foreach($leads as $lead)
                                <li class="d-flex align-items-center justify-content-between">
                                    <div class="row w-100 d-flex align-items-center justify-content-between">

                                        <div class="col-md-2 col-6">
                                            <div class="product-title">{{$lead->name}}</div>
                                        </div>

                                        <div class="col-md-4 col-6 d-md-flex d-none flex-column">
                                            <span>
                                                {{$lead->lastname}}
                                            </span>
                                            {{-- <div class="subtitle-info">{{$lead->email}}</div> --}}
                                        </div>

                                        <div class="col-md-4 col-3 d-flex flex-column">
                                            <span>{{$lead->company}}</span>
                                            {{-- <div class="subtitle-info">{{$lead->address_complement}}</div> --}}
                                        </div>

                                        <div class="col-md-1 col-6 d-md-flex d-none align-items-center">
                                            <!-- Default switch -->
                                            <div class="custom-control custom-switch">
                                              <input type="checkbox" class="custom-control-input" id="customSwitches_{{$lead->id}}" @if($lead->delivery_status == 1) checked="" @endif>
                                              <input type="hidden" class="row_id" value="{{$lead->id}}">
                                              <label class="custom-control-label" for="customSwitches_{{$lead->id}}"></label>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>

<script>
    $('.custom-control-input').change(function(){
        lead_id = $(this).parent().children('.row_id').val();

        if($(this).prop("checked") == true){
            status = 1;
        }else{
            status = 0;
        }

        $.get('/lead/updatedeliverystatus/'+lead_id+'/'+status);
    })
</script>