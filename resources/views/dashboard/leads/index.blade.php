<x-app-layout>
    <x-slot name="header">
        <div class="d-flex align-items-center justify-content-between">
            <h2 class="font-semibold text-xl text-gray-800 leading-tight mb-0" id="new-page-title-h3">
                Produtos
            </h2>
        </div>
    </x-slot>

    <div class="pagina container">
        <div class="row mt-5 index-section">
            <div class="d-flex flex-column flex-md-row w-100">
                <div class="col-12 col-md-12">
                    <div class="list-stack">
                        <ul>
                            <li class="d-flex align-items-center justify-content-between">
                                <div class="p-0 col-md-6 col-6">
                                    Produto
                                </div>
                                <div class="p-0 col-md-5 col-6 d-md-block d-none align-items-center">
                                    Página
                                </div>
                                <div class="p-0 col-md-1 col-1 d-md-block d-none justify-content-end pr-0 align-items-center">
                                </div>
                            </li>
                            @foreach($products as $product)
                                <li class="d-flex align-items-center justify-content-between">
                                    <div class="row w-100">
                                        <div class="col-md-6 col-6">
                                            <div class="product-title">{{$product->title}}</div>
                                        </div>
                                        <div class="col-md-5 col-6 d-md-flex d-none align-items-center">
                                            <div class="d-flex align-items-center">
                                                <a class="subtitle-info d-md-block d-none" href="{{env('APP_URL')}}/{{$product->slug}}">{{env('APP_URL')}}/{{$product->slug}}</a>
                                            </div>
                                        </div>
                                        <div class="col-md-1 col-3 d-flex justify-content-end pr-0 align-items-end">
                                            @if($product->leads->count() > 0)
                                                <a href="/dashboard/pedidos/pedidos-por-produtos/{{$product->id}}">Pedidos</a>
                                            @else
                                                <span class="subtitle-info"><i>Sem pedidos</i></span>
                                            @endif
                                        </div>
                                    </div>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>

<script>

</script>