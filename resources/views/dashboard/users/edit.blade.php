<x-guest-layout>
    <x-jet-authentication-card>
        <x-slot name="logo">
            <x-jet-authentication-card-logo />
        </x-slot>

        <x-jet-validation-errors class="mb-4" />

        <form method="POST" action="/dashboard/usuarios/update/{{$user->id}}">
            @csrf

            @if(Auth::user()->role == 'admin')
                <div>
                    <x-jet-label value="Nome" />
                    <input class="form-input rounded-md shadow-sm block mt-1 w-full" type="text" name="name" required="" value="{{$user->name}}">
                </div>

                <div class="mt-4">
                    <x-jet-label value="E-mail" />
                    <input class="form-input rounded-md shadow-sm block mt-1 w-full" type="email" name="email" required="" value="{{$user->email}}">
                </div>

                @if($user->id != Auth::user()->id )
                    <div class="mt-4">
                        <label for="role">Tipo de usuário</label>
                        <select class="form-input rounded-md shadow-sm block mt-1 w-full" required name="role" id="role">
                            <option value="blmbrg" @if($user->role == 'blmbrg') selected @endif>Bloomberg</option>
                            <option value="outsource" @if($user->role == 'outsource') selected @endif>Logística</option>
                        </select>
                    </div>
                @endif

                <div class="mt-4">
                    <label for="country">País</label>
                    <select class="form-input rounded-md shadow-sm block mt-1 w-full" required name="country" id="country">
                        <option value="Argentina" @if($user->country == 'Argentina') selected @endif>Argentina</option>
                        <option value="Brasil" @if($user->country == 'Brasil') selected @endif>Brasil</option>
                        <option value="Chile" @if($user->country == 'Chile') selected @endif>Chile</option>
                        <option value="Colombia" @if($user->country == 'Colombia') selected @endif>Colombia</option>
                        <option value="México" @if($user->country == 'México') selected @endif>México </option>
                        <option value="Perú" @if($user->country == 'Perú') selected @endif>Perú</option>
                        <option value="Uruguay" @if($user->country == 'Uruguay') selected @endif>Uruguay</option>
                    </select>
                </div>
            @endif
            
            @if(Auth::user()->id == $user->id && Auth::user()->role != 'admin')
                <h5>{{Auth::user()->name}}</h5>
            @endif

            @if(Auth::user()->id == $user->id)
                <div class="mt-4">
                    <x-jet-label value="Nova senha" />
                    <x-jet-input class="block mt-1 w-full" type="password" name="password" required autocomplete="new-password" />
                </div>

                <div class="mt-4">
                    <x-jet-label value="Confirmar nova senha" />
                    <x-jet-input class="block mt-1 w-full" type="password" name="password_confirmation" required autocomplete="new-password" />
                </div>
            @endif

            <div class="flex items-center justify-content-between mt-4">
                <a href="#" onclick="window.history.back()" class="inline-flex items-center px-4 py-2 bg-gray-800 border border-transparent rounded-md font-semibold text-xs text-white uppercase tracking-widest hover:bg-gray-700 active:bg-gray-900 focus:outline-none focus:border-gray-900 focus:shadow-outline-gray disabled:opacity-25 transition ease-in-out duration-150">
                    Cancelar
                </a>

                <x-jet-button class="ml-4">
                    Salvar
                </x-jet-button>
            </div>
        </form>
    </x-jet-authentication-card>
</x-guest-layout>
