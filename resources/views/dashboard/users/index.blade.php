<x-app-layout>
    <x-slot name="header">
        <div class="d-flex align-items-center justify-content-between">
            <h2 class="font-semibold text-xl text-gray-800 leading-tight mb-0" id="new-page-title-h3">
                Usuários
            </h2>
            <a href="/dashboard/usuarios/novo-usuario">Novo usuário</a>
        </div>
    </x-slot>

    <div class="pagina container">
        <div class="row mt-5 index-section">
            <div class="d-flex flex-column flex-md-row w-100">
                <div class="col-12 col-md-12">
                    <div class="list-stack">
                        <ul>
                            <li class="d-flex align-items-center justify-content-between">
                                <div class="row w-100">
                                    <div class="col-md-6 col-6">
                                        Usuário
                                    </div>
                                    <div class="col-md-3 col-6 d-md-block d-none">
                                        Tipo de usuário
                                    </div>
                                    <div class="col-md-2 col-6 d-md-block">
                                        País
                                    </div>
                                    <div class="col-md-1 col-6 d-md-block d-none align-items-center">
                                        Editar
                                    </div>
                                </div>
                            </li>
                            @foreach($users as $user)
                                <li class="d-flex align-items-center justify-content-between">
                                    <div class="row w-100">
                                        <div class="col-md-6 col-6">
                                            <div class="product-title d-flex align-items-center">
                                                <span>{{$user->name}}</span>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-6 d-md-flex d-none">
                                            <div class="d-flex align-items-center">
                                                @switch($user->role)
                                                    @case('blmbrg')
                                                        Bloomberg
                                                        @break
                                                    @case('outsource')
                                                        Logística
                                                        @break
                                                    @case('admin')
                                                        Administrador
                                                        @break
                                                @endswitch
                                            </div>
                                            {{-- <div class="subtitle-info">Códigos disponiveis</div> --}}
                                        </div>
                                        <div class="col-md-2 col-6 d-flex">
                                            {{$user->country}}
                                        </div>
                                        <div class="col-md-1 col-6 d-flex pr-0">
                                            <a href="/dashboard/usuarios/editar/{{$user->id}}">Editar</a>
                                        </div>
                                    </div>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>

<script>

</script>