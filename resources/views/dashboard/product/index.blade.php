<x-app-layout>
    <x-slot name="header">
        <div class="d-flex align-items-center justify-content-between">
            <h2 class="font-semibold text-xl text-gray-800 leading-tight mb-0" id="new-page-title-h3">
                Páginas
            </h2>
            <a href="/dashboard/paginas/nova">Nova página</a>
        </div>
    </x-slot>

    <div class="pagina container">
        <div class="row mt-5 index-section">
            <div class="d-flex flex-column flex-md-row w-100">
                <div class="col-12 col-md-12">
                    <div class="list-stack">
                        <ul>
                            <li class="d-flex align-items-center justify-content-between">
                                <div class="p-0 col-md-6 col-6">
                                    Página
                                </div>
                                <div class="p-0 col-md-2 col-6 d-md-block d-none align-items-center">
                                    Códigos
                                </div>
                                <div class="p-0 col-md-1 col-6 d-md-block align-items-center">
                                    Status
                                </div>
                                <div class="p-0 col-md-1 col-6 d-md-block d-none align-items-center">
                                    Criação
                                </div>
                                <div class="p-0 col-md-1 col-6 d-md-block d-none align-items-center">
                                    Atualização
                                </div>
                                <div class="p-0 col-md-1 col-6 d-md-block d-none justify-content-end pr-0 align-items-center">
                                </div>
                            </li>
                            @foreach($products as $product)
                                <li class="d-flex align-items-center justify-content-between">
                                    <div class="row w-100">
                                        <div class="col-md-6 col-6">
                                            <div class="product-title d-flex align-items-center">
                                                @if($product->main_product == 1)
                                                    <span class="main-page-active mr-2"></span>
                                                @endif
                                                <span>{{$product->title}}</span>
                                            </div>
                                            <a class="subtitle-info d-md-block d-none" target="_blank" href="{{env('APP_URL')}}/{{$product->slug}}">{{env('APP_URL')}}/{{$product->slug}}</a>
                                        </div>
                                        <div class="col-md-2 col-6 d-md-flex d-none align-items-center">
                                            <div class="d-flex align-items-center">
                                                @if($product->codes->where('status',0)->count() > 1)
                                                    <span>{{$product->codes->where('status',0)->count()}}</span>
                                                    <span class="subtitle-info-2 ml-2">Códigos<br>disponiveis</span>
                                                @elseif($product->codes->where('status',0)->count() == 1)
                                                    <span>{{$product->codes->where('status',0)->count()}}</span>
                                                    <span class="subtitle-info-2 ml-2">Código<br>disponível</span>
                                                @else
                                                    <span class="status-alert">Indisponível</span>
                                                @endif
                                            </div>
                                            {{-- <div class="subtitle-info">Códigos disponiveis</div> --}}
                                        </div>
                                        <div class="col-md-1 col-3 d-flex align-items-center">
                                            @switch($product->status)
                                                @case(1)
                                                    <span class="status-active">Ativa</span>
                                                    @break
                                                @case(0)
                                                    <span class="status-inactive">Desativada</span>
                                                    @break
                                            @endswitch
                                        </div>
                                        <div class="col-md-1 col-6 d-md-flex d-none align-items-center">
                                            <span class="subtitle-info">
                                                {{$product->created_at->format('d/m/Y')}}
                                            </span>
                                        </div>
                                        <div class="col-md-1 col-6 d-md-flex d-none align-items-center">
                                            <span class="subtitle-info">
                                                {{$product->updated_at->format('d/m/Y')}}
                                            </span>
                                        </div>
                                        <div class="col-md-1 col-3 d-flex justify-content-end pr-0 align-items-center">
                                            <a href="/dashboard/paginas/editar/{{$product->slug}}">Editar</a>
                                        </div>
                                    </div>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>

<script>

</script>