<x-app-layout>
    <x-slot name="header">
        <div class="d-flex align-items-center">
            <a href="/dashboard/paginas">
                <img src="{{asset('images/arrow-back.png')}}" class="w-50">
            </a>
            <h2 class="font-semibold text-xl text-gray-800 leading-tight mb-0" id="new-page-title-h3">
                {{$product->title}} - {{$product->country}}
            </h2>
        </div>
    </x-slot>

    <div class="pagina container">
        <form id="update-page-form" action="/dashboard/paginas/atualizar/{{$product->id}}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="row mt-md-5 section">
                <div class="d-flex flex-column flex-md-row w-100">
                    <div class="col-12 col-md-4 d-flex flex-column section-title">
                        <h3>Informações Básicas da página</h3>
                        <p>Cada página deve ter um slug único</p>
                    </div>
                    <div class="col-12 col-md-8">
                        <div class="form-block">
                            <div class="form-group d-flex flex-column">
                                <label for="new-page-title">Título da página *</label>
                                <input type="text" name="title" id="new-page-title" required="" value="{{$product->title}}">
                            </div>
                            <div class="form-group d-flex flex-column">
                                <label for="slug">Slug da página <span class="invalid-slug"><i>A URL formada por este slug já está em uso. Por favor, escolha outro.</i></span></label>
                                <div class="half-input d-flex flex-row">
                                    <input tabindex="-1" type="text" name="" id="" value="{{env('APP_URL')}}" readonly="">
                                    <input type="text" name="slug" id="slug" value="{{$product->slug}}" required="">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row d-flex align-items-end">
                                    <div class="col-6 col-md-3 d-flex flex-column">
                                        <label for="main">Página principal</label>
                                        <select type="checkbox" name="main" id="main" required="">
                                            <option @if($product->main_product == 1) selected @endif value="1">Sim</option>
                                            <option @if($product->main_product == 0) selected @endif value="0">Não</option>
                                        </select>
                                    </div>
                                    <div class="col-6 col-md-3 d-flex flex-column">
                                        <label for="status">Status</label>
                                        <select type="checkbox" name="status" id="status" required="">
                                            <option @if($product->status == 1) selected @endif value="1">Ativa</option>
                                            <option @if($product->status == 0) selected @endif value="0">Desativada</option>
                                        </select>
                                    </div>
                                    <div class="col-6 col-md-3 d-flex flex-column">
                                        <label for="code-amount">Códigos *</label>
                                        <input type="number" min="{{$codes['total']}}" name="code-amount" id="code-amount" required="" value="{{$codes['total']}}">
                                    </div>

                                    @if($product->codes->count() > 0)
                                        <div class="col-6 col-md-3 d-flex flex-column">
                                            <a class="show-codes" onclick="showCodes({{$product->id}})">Ver códigos</a>
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row mt-md-5 section">
                <div class="d-flex flex-column flex-md-row w-100">
                    <div class="col-12 col-md-4 d-flex flex-column section-title">
                        <h3>Imagem</h3>
                        <p>O tamanho ideal para a imagem é de 1024px x 600px</p>
                    </div>
                    <div class="col-12 col-md-8">
                        <div class="form-block">
                            <div class="form-group d-flex flex-column">
                                <img src="{{asset('images/products')}}/{{$product->image}}">
                                <label for="image">Atualizar a imagem</label>
                                <input type="file" name="image" id="image">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row mt-md-5 section">
                <div class="d-flex flex-column flex-md-row w-100">
                    <div class="col-12 col-md-4 d-flex flex-column section-title">
                        <h3>Descrição</h3>
                    </div>
                    <div class="col-12 col-md-8">
                        <div class="form-block">
                            <div class="form-group d-flex flex-column">
                                <label for="description">Descrição do produto</label>
                                <textarea name="description" id="description">{{$product->description}}</textarea>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row mt-md-5 section">
                <div class="d-flex flex-column flex-md-row w-100">
                    <div class="col-12 col-md-4 d-flex flex-column section-title">
                        <h3>Termos de privacidade</h3>
                    </div>
                    <div class="col-12 col-md-8">
                        <div class="form-block">
                            <div class="form-group d-flex flex-column">
                                {{-- <label for="description"></label> --}}
                                <textarea name="privacy_terms" id="privacy_terms">{{$product->privacy_terms}}</textarea>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row mt-md-5 section">
                <div class="d-flex flex-column flex-md-row w-100 justify-content-end">
                    <div class="form-group">
                        @if($product->codes->where('status',1)->count() == 0)
                            <a href="/dashboard/paginas/deletar/{{$product->id}}" class="danger mr-3" type="submit">Deletar página</a>
                        @endif
                    </div>
                    <div class="form-group">
                        <button class="success" type="submit">Salvar atualizações</button>
                    </div>
                </div>
            </div>

        </form>
    </div>
</x-app-layout>

<!-- Modal -->
<div class="modal fade modal-codes" id="modal-codes" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title">{{$product->title}}</h3>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12 col-sm-12 text-center" id="code-modal-body">

                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <a class="" href="/dashboard/paginas/codestospreadsheet/{{$product->id}}">Exportar para Excel</a>
            </div>
        </div>

    </div>
</div>

<script>
    var codesList = '';

    function showCodes(pageId){
        $.get('/dashboard/paginas/getcodes/' + pageId, function(data){

            codesList = '<table><thead><tr><th class="w-25">Código</th><th>Nome</th><th>Sobrenome</th><th>Empresa</th><th>Entrega</th></tr></thead><tbody>';
            $.each(data, function(index, codeData){
                if(codeData.status == 1){
                    codesList = codesList + '<tr><td><span class="invalid-code">'+codeData.code+'</span></td><td>'+codeData.lead.name+'</td><td>'+codeData.lead.lastname+'</td><td>'+codeData.lead.company+'</td>';
                        if(codeData.lead.delivery_status == 1){
                            codesList = codesList + '<td align="middle"><img src="/images/check.png"></td></tr>';
                        }else{
                            codesList = codesList +'</tr>';
                        }
                }else{
                    codesList = codesList + '<tr><td><span class="valid-code">'+codeData.code+'</span></td><td></td><td></td><td></td></tr>';
                }
            })
            codesList = codesList + '</tbody></table>';

            $('#code-modal-body').html(codesList);

            $('#modal-codes').modal()
        });
    }

    $('#new-page-title').keyup(function(){
        var slug = $(this).val();
        slug = slug.slugify();

        $('#slug').val(slug);

        if ($(this).val() != null && $(this).val() != undefined && $(this).val() != '') {
            $('#new-page-title-h3').html($(this).val());
        }else{
            $('#new-page-title-h3').html('Nova página');
        }

    });

    $('#slug').keyup(function(){
        $('#slug').removeClass('invalid');
        $('.invalid-slug').fadeOut();
    })

    var previousSlug = $('#slug').val();

    $('#update-page-form').submit(function(){
        slug = $('#slug').val();

        if (previousSlug == slug) {
            $('#update-page-form').unbind().submit();
        }else{
            $.get('/dashboard/paginas/checkslug/' + slug, function(data){
                if (data == 1 || data == true ) {
                    $('#slug').addClass('invalid');
                    $('.invalid-slug').fadeIn();
                }else{
                    $('#update-page-form').unbind().submit();
                }
            });
        }

        return false;
    })
</script>