<x-app-layout>
    <x-slot name="header">
        <div class="d-flex align-items-center">
            <a href="/dashboard/paginas">
                <img src="{{asset('images/arrow-back.png')}}" class="w-50">
            </a>
            <h2 class="font-semibold text-xl text-gray-800 leading-tight mb-0" id="new-page-title-h3">
                Nova página
            </h2>
        </div>
    </x-slot>

    <div class="pagina container">
        <form id="new-page-form" action="/dashboard/paginas/nova/salvar" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="row mt-md-5 section">
                <div class="d-flex flex-column flex-md-row w-100">
                    <div class="col-12 col-md-4 d-flex flex-column section-title">
                        <h3>Informações gerais</h3>
                        <p>Cada página deve ter um slug único</p>
                    </div>
                    <div class="col-12 col-md-8">
                        <div class="form-block">
                            <div class="form-group d-flex flex-column">
                                <label for="new-page-title">Título da página *</label>
                                <input type="text" name="title" id="new-page-title" required="">
                            </div>
                            <div class="form-group d-flex flex-column">
                                <label for="slug">Slug da página *  <span class="invalid-slug"><i>A URL formada por este slug já está em uso. Por favor, escolha outro.</i></span></label>
                                <div class="half-input d-flex flex-row">
                                    <input tabindex="-1" type="text" name="" id="" value="{{env('APP_URL')}}" readonly="">
                                    <input type="text" name="slug" id="slug" required="">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-12 col-md-4 d-flex flex-column">
                                        <label for="main">Página principal</label>
                                        <select type="checkbox" name="main" id="main" required="">
                                            <option value="1">Sim</option>
                                            <option value="0" selected="">Não</option>
                                        </select>
                                    </div>
                                    <div class="col-12 col-md-4 d-flex flex-column">
                                        <label for="code-amount">Quantidade de códigos *</label>
                                        <input type="number" min="0" name="code-amount" id="code-amount" required="" value="30">
                                    </div>
                                    <div class="col-12 col-md-4 d-flex flex-column">
                                        <label for="code-amount">País</label>
                                        <select id="product_country" name="product_country">
                                            <option value="Argentina">Argentina</option>
                                            <option value="Brasil">Brasil</option>
                                            <option value="Chile">Chile</option>
                                            <option value="Colombia">Colombia</option>
                                            <option value="México">México </option>
                                            <option value="Perú">Perú</option>
                                            <option value="Uruguay">Uruguay</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row mt-md-5 section">
                <div class="d-flex flex-column flex-md-row w-100">
                    <div class="col-12 col-md-4 d-flex flex-column section-title">
                        <h3>Imagem</h3>
                        <p>O tamanho ideal para a imagem é de 1024px x 600px</p>
                    </div>
                    <div class="col-12 col-md-8">
                        <div class="form-block">
                            <div class="form-group d-flex flex-column">
                                <label for="image">Selecione a imagem *</label>
                                <input type="file" name="image" id="image" required="">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row mt-md-5 section">
                <div class="d-flex flex-column flex-md-row w-100">
                    <div class="col-12 col-md-4 d-flex flex-column section-title">
                        <h3>Descrição</h3>
                    </div>
                    <div class="col-12 col-md-8">
                        <div class="form-block">
                            <div class="form-group d-flex flex-column">
                                <label for="description">Descrição do produto *</label>
                                <textarea name="description" id="description" required=""></textarea>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row mt-md-5 section">
                <div class="d-flex flex-column flex-md-row w-100">
                    <div class="col-12 col-md-4 d-flex flex-column section-title">
                        <h3>Termos de privacidade</h3>
                    </div>
                    <div class="col-12 col-md-8">
                        <div class="form-block">
                            <div class="form-group d-flex flex-column">
                                {{-- <label for="description"></label> --}}
                                <textarea name="privacy_terms" id="privacy_terms"></textarea>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row mt-md-5 section">
                <div class="d-flex flex-column flex-md-row w-100 justify-content-end">
                    <div class="form-group">
                        <button class="success" type="submit">Criar página</button>
                    </div>
                </div>
            </div>

        </form>
    </div>
</x-app-layout>

<script>

    $('#new-page-title').keyup(function(){
        var slug = $(this).val();
        slug = slug.slugify();

        $('#slug').val(slug);

        if ($(this).val() != null && $(this).val() != undefined && $(this).val() != '') {
            $('#new-page-title-h3').html($(this).val());
        }else{
            $('#new-page-title-h3').html('Nova página');
        }

    });

    $('#slug').keyup(function(){
        $('#slug').removeClass('invalid');
        $('.invalid-slug').fadeOut();
    })

    $('#new-page-form').submit(function(){
        slug = $('#slug').val();

        $.get('/dashboard/paginas/checkslug/' + slug, function(data){
            if (data == 1 || data == true ) {
                $('#slug').addClass('invalid');
                $('.invalid-slug').fadeIn();
            }else{
                $('#new-page-form').unbind().submit();
            }
        });
        
        return false;
    })
</script>