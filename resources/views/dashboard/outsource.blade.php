<x-app-layout>
    <x-slot name="header">
        <div class="d-flex align-items-center justify-content-between">
            <h2 class="font-semibold text-xl text-gray-800 leading-tight">
                Leads
            </h2>
            <a href="/leads/spreadsheet/1">
                Exportar
            </a>
        </div>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg content-body">
               <table>
                    <thead>
                        <tr>
                            <th>Id</th>
                            <th>Nome</th>
                            <th>Contato</th>                            
                            <th>Endereço</th>
                            <th>Cidade</th>
                            <th>Estado</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($leads as $lead)
                            <tr>
                                <td>{{$lead->id}}</td>
                                <td>{{$lead->name}} {{$lead->lastname}}</td>
                                <td>{{$lead->phone}} <br><span class="info-2nd">{{$lead->email}}</span></td>
                                <td>{{$lead->address}}<br><span class="info-2nd">{{$lead->address_complement}}</span></td>
                                <td>{{$lead->city}}</td>
                                <td>{{$lead->state}}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                <div class="pt-3">
                    {{$leads->render()}}
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
