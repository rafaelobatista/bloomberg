<x-guest-layout>
    <x-jet-authentication-card>
        <x-slot name="logo">
            <x-jet-authentication-card-logo />
        </x-slot>

        <x-jet-validation-errors class="mb-4" />

        <form method="POST" action="{{ route('register') }}">
            @csrf

            <div>
                <x-jet-label value="Nome" />
                <x-jet-input class="block mt-1 w-full" type="text" name="name" :value="old('name')" required autofocus autocomplete="name" />
            </div>

            <div class="mt-4">
                <x-jet-label value="E-mail" />
                <x-jet-input class="block mt-1 w-full" type="email" name="email" :value="old('email')" required />
            </div>

            <div class="mt-4">
                <label for="role">Tipo de usuário</label>
                <select class="form-input rounded-md shadow-sm block mt-1 w-full" required name="role" id="role">
                    <option value="bloomberg">Bloomberg</option>
                    <option value="outsource">Logística</option>
                    <option value="admin">Administrador</option>
                </select>
            </div>

            <div class="mt-4">
                <label for="country">País</label>
                <select class="form-input rounded-md shadow-sm block mt-1 w-full" required name="country" id="country">
                    <option value="Argentina">Argentina</option>
                    <option value="Brasil">Brasil</option>
                    <option value="Chile">Chile</option>
                    <option value="Colombia">Colombia</option>
                    <option value="México">México </option>
                    <option value="Perú">Perú</option>
                    <option value="Uruguay">Uruguay</option>
                </select>
            </div>

            <div class="mt-4">
                <x-jet-label value="Senha" />
                <x-jet-input class="block mt-1 w-full" type="password" name="password" required autocomplete="new-password" />
            </div>

            <div class="mt-4">
                <x-jet-label value="Confirmar senha" />
                <x-jet-input class="block mt-1 w-full" type="password" name="password_confirmation" required autocomplete="new-password" />
            </div>

            <div class="flex items-center justify-end mt-4">
                {{-- <a class="underline text-sm text-gray-600 hover:text-gray-900" href="{{ route('login') }}">
                    Já possui usuário?
                </a> --}}

                <x-jet-button class="ml-4">
                    Salvar
                </x-jet-button>
            </div>
        </form>
    </x-jet-authentication-card>
</x-guest-layout>
