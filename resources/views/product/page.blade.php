@extends('layouts.blmbrg')

@section('content')

    <div class="page container">
        <section id="code-validation" class="mb-5">
            <div class="row mt-5">
                <div class="d-flex flex-column flex-md-row w-100 justify-content-center">
                    <div class="col-12 col-md-7">
                        <h2 class="mb-5">Please enter your code.</h2>
                        <div class="code-box">
                            {{-- <h3 class="mb-4">
                                Enter the CODE below to redeem your gift.
                            </h3> --}}
                            <p class="mb-4">
                                Input the unique and non-transferable code, which you receive by email.
                            </p>
                            <div class="d-flex justify-content-center">
                                <input class="" type="" name="" id="code-to-validate">
                                <button id="validate-code" class="w-lg-25 w-md-50">Submit</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section id="form" class="form-section">
            <div class="row mt-4">
                <div class="d-flex flex-column flex-md-row w-100 justify-content-center">
                    <div class="col-12 col-md-5">
                        <img class="product-image" src="{{asset('images/products')}}/{{$product->image}}">
                    </div>
                    <div class="col-12 col-md-7">
                        <h2 class="mb-3">{{$product->title}}</h2>
                        <p>{!!$product->description!!}</p>
                    </div>
                </div>
            </div>
            <div class="row mt-5 d-flex form-title">
                <div class="col-md-12 col-12">
                    <h3>Fill out the form below to redeem your item.</h3>
                </div>
            </div>
            <div class="row mt-5 mb-5">
                <div class="d-flex flex-column flex-md-row w-100 justify-content-center">
                    <div class="col-12 col-md-8">
                        <form id="lead-form" class="lead-form">
                            @csrf
                            <input type="hidden" name="used_code" id="used_code">
                            <input type="hidden" name="product_id" id="product_id" value="{{$product->id}}">
                            <div class="form-row">
                                <div class="form-group col-md-6 col-12">
                                    <label for="name">Name*</label>
                                    <input type="text" name="name" id="name" class="form-control" required="">
                                </div>
                                <div class="form-group col-md-6 col-12">
                                    <label for="lastname">Last name*</label>
                                    <input type="text" name="lastname" id="lastname" class="form-control" required="">
                                </div>
                                <div class="form-group col-md-12 col-12">
                                    <label for="company">Company*</label>
                                    <input type="text" name="company" id="company" class="form-control" required="">
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-6 col-12">
                                    <label for="email">E-mail*</label>
                                    <input type="email" name="email" id="email" class="form-control" required="">
                                </div>
                                <div class="form-group col-md-6 col-12">
                                    <label for="phone">Phone*</label>
                                    <input type="text" name="phone" id="phone" class="form-control" required="">
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-8 col-12">
                                    <label for="address">Delivery address*</label>
                                    <input type="text" name="address" id="address" class="form-control" required="">
                                </div>
                                <div class="form-group col-md-4 col-12">
                                    <label for="address_complement">Complement</label>
                                    <input type="text" name="address_complement" id="address_complement" class="form-control">
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-3 col-12">
                                    <label for="country">Country*</label>
                                    <select name="country" id="country" class="form-control" required="">
                                        <option selected="" id="hide"></option>
                                        <option value="Argentina">Argentina</option>
                                        <option value="Brasil">Brasil</option>
                                        <option value="Chile">Chile</option>
                                        <option value="Colombia">Colombia</option>
                                        <option value="México">México </option>
                                        <option value="Perú">Perú</option>
                                        <option value="Uruguay">Uruguay</option>
                                    </select>
                                </div>
                                <div class="form-group col-md-3 col-12">
                                    <label for="state">State*</label>
                                    <select name="state" id="state" class="form-control" required="">
                                        
                                    </select>
                                </div>
                                <div class="form-group col-md-3 col-12">
                                    <label for="city">City*</label>
                                    <input type="text" name="city" id="city" class="form-control" required="">
                                </div>
                                <div class="form-group col-md-3 col-12">
                                    <label for="postal_code">ZIP Code*</label>
                                    <input type="text" name="postal_code" id="postal_code" class="form-control" required="">
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-6 col-12">
                                    <button class="">Redeem</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>

        <section id="response" class="response-section">
            <div class="row mt-5">
                <div class="d-flex flex-column flex-md-row w-100 justify-content-center">
                    <div class="col-12 col-md-12 text-center response">
                        <h2 class="mb-2">Thank you very much. Soon you will receive your item.</h2>
                        <p>Delivery is expected within 15 working days.</p>
                    </div>
                </div>
            </div>
        </section>
    </div>

    <div class="modal fade modal-codes" id="invalid-code-modal" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 text-center">
                            <h3>This code is no longer valid</h3>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <script>
        $('#phone').mask('(00)00000-0000');
        $('#postal_code').mask('00000-000');

        $('#validate-code').click(function(){

            var code = $('#code-to-validate').val();
            var product_id = $('#product_id').val();

            $.get('/code/' + code +'/'+ product_id, function(response){

                if (response == 'invalid') {
                    $('#invalid-code-modal').modal()
                }else{
                    $('#code-validation').slideUp();
                    $('#form').fadeIn();

                    $('#used_code').val(code);
                }
            });
        });

        $('#lead-form').submit(function(){
            var form_data = $(this).serializeArray();

            $.post('/lead/createlead', form_data, function(response){
                if(response == true){
                    $('#form').slideUp();
                    $('#response').fadeIn();
                }
            })

            return false;
        });


        $('#country').change(function(){
            var stateList ='';
            
            $.get('/getstates/' + $(this).val(),function(response){
                
                $('#hide').hide();

                $.each(response, function(index, state){
                    stateList = stateList + '<option value="'+state.state+'">'+state.state+'</option>';
                })
                
                $('#state').html(stateList);
            })

        })
    </script>

@endsection
