<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/




// ========== ROTAS DASHBOARD ========== //

Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard/', 'AdminController@index')->name('dashboard');

// Rotas usuário bloomberg - PRODUCT CONTROLLER
Route::middleware(['auth:sanctum', 'verified'])->get('dashboard/paginas/', 'ProductController@index');
Route::middleware(['auth:sanctum', 'verified'])->get('dashboard/paginas/nova/', 'ProductController@create');
Route::middleware(['auth:sanctum', 'verified'])->post('dashboard/paginas/nova/salvar', 'ProductController@store');
Route::middleware(['auth:sanctum', 'verified'])->get('dashboard/paginas/editar/{slug}', 'ProductController@edit');
Route::middleware(['auth:sanctum', 'verified'])->post('dashboard/paginas/atualizar/{product}', 'ProductController@update');
Route::middleware(['auth:sanctum', 'verified'])->get('dashboard/paginas/checkslug/{slug}', 'ProductController@checkSlug');
Route::middleware(['auth:sanctum', 'verified'])->get('dashboard/paginas/getcodes/{page}', 'ProductController@getCodes');
Route::middleware(['auth:sanctum', 'verified'])->get('dashboard/paginas/codestospreadsheet/{page_id}', 'CodeController@codesToSpreadsheet');
Route::middleware(['auth:sanctum', 'verified'])->get('dashboard/paginas/deletar/{page_id}', 'ProductController@delete');

Route::middleware(['auth:sanctum', 'verified'])->get('dashboard/usuarios/', 'UserController@index');
Route::middleware(['auth:sanctum', 'verified'])->get('dashboard/usuarios/editar/{id}', 'UserController@edit');
Route::middleware(['auth:sanctum', 'verified'])->post('dashboard/usuarios/update/{id}', 'UserController@update');
Route::middleware(['auth:sanctum', 'verified'])->get('dashboard/usuarios/novo-usuario', 'UserController@create');
Route::middleware(['auth:sanctum', 'verified'])->post('dashboard/usuarios/store', 'UserController@store');


// Rotas usuário logistica - PRODUCT CONTROLLER
Route::middleware(['auth:sanctum', 'verified'])->get('dashboard/pedidos/spreadsheet/{campaign}', 'LeadController@spreadsheet');
Route::middleware(['auth:sanctum', 'verified'])->get('dashboard/pedidos', 'LeadController@index');
Route::middleware(['auth:sanctum', 'verified'])->get('dashboard/pedidos/pedidos-por-produtos/{product_id}', 'LeadController@leadsByProduct');

// ========== FIM ROTAS DASHBOARD ========== //


// ========== ROTAS PÚBLICAS ========== //

Route::get('/', 'ProductController@showMainPage');
Route::get('/{pagina_slug}', 'ProductController@showPage');
Route::get('/code/{code}/{product_id}', 'CodeController@validateCode');
Route::post('/lead/createlead', 'LeadController@store');
Route::get('/lead/updatedeliverystatus/{lead_id}/{status}', 'LeadController@updateDeliveryStatus');
Route::get('/getstates/{country}', 'AdminController@getStates');

// ========== ROTAS PÚBLICAS ========== //