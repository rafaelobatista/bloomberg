<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLeadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('leads', function (Blueprint $table) {
            $table->id();
            $table->char('name')->nullable();
            $table->char('lastname')->nullable();
            $table->char('company')->nullable();
            $table->char('email')->nullable();
            $table->char('phone',14)->nullable();
            $table->char('address')->nullable();
            $table->char('address_complement')->nullable();
            $table->char('city')->nullable();
            $table->char('district')->nullable();
            $table->char('state')->nullable();
            $table->char('postal_code',9)->nullable();
            $table->char('product_id')->nullable();
            $table->char('promotional_code')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('leads');
    }
}
