<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Models\Code;
use App\Models\Country;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Auth::user()->role != 'blmbrg') {
            return redirect('/dashboard');
        }

        $products = Product::all();

        return view('dashboard.product.index',[
            'products' => $products,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (Auth::user()->role != 'blmbrg') {
            return redirect('/dashboard');
        }

        return view('dashboard.product.form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (Auth::user()->role != 'blmbrg') {
            return redirect('/dashboard');
        }

        $imageName = $request->input('slug') .'-'. time().'.'.$request->image->extension();
        $request->image->move(public_path('images/products'), $imageName);

        $product = new Product;
        $product->title = $request->input('title');
        $product->slug = $request->input('slug');
        $product->image = $imageName;
        $product->description = $request->input('description');
        $product->privacy_terms = $request->input('privacy_terms');
        $product->country = $request->input('product_country');
        
        if($request->input('main') == 1){
            $previus_main_product = Product::where('main_product',1)->first();

            if($previus_main_product){
                $previus_main_product->main_product = 0;
                $previus_main_product->save();
            }
        }
        
        $product->main_product = $request->input('main');

        $product->save();
        
        $code_amount = $request->input('code-amount');
        for ($i=0; $i < $code_amount; $i++) { 
            $code = new Code;
            $code->product_id = $product->id;
            $code->code = uniqid(bin2hex(random_bytes(3)));
            $code->save();
        }

        return redirect('/dashboard/paginas');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit($slug)
    {
        if (Auth::user()->role != 'blmbrg') {
            return redirect('/dashboard');
        }

        $product = Product::where('slug',$slug)->first();

        $codes['total'] = $product->codes->count();
        $codes['used'] = $product->codes->where('status',1)->count();
        $codes['available'] = $product->codes->where('status',0)->count();
        
        return view('dashboard.product.edit',[
            'product' => $product,
            'codes' => $codes,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {
        if (Auth::user()->role != 'blmbrg') {
            return redirect('/dashboard');
        }

        $product->title = $request->input('title');
        $product->slug = $request->input('slug');
        $product->description = $request->input('description');
        $product->privacy_terms = $request->input('privacy_terms');
        $product->status = $request->input('status');
        
        $balance_amount = $request->input('code-amount') - $product->codes->count();

        if($balance_amount > 0){
            for ($i=0; $i < $balance_amount; $i++) { 
                $code = new Code;
                $code->product_id = $product->id;
                $code->code = uniqid(bin2hex(random_bytes(3)));
                $code->save();
            }
        }

        if($request->input('main') == 1){
            $previus_main_product = Product::where('main_product',1)->first();

            if($previus_main_product){
                $previus_main_product->main_product = 0;
                $previus_main_product->save();
            }
        }

        $product->main_product = $request->input('main');

        if($request->image){
            $imageName = $request->input('slug') .'-'. time().'.'.$request->image->extension();
            
            $request->image->move(public_path('images/products'), $imageName);
            $product->image = $imageName;
        }

        $product->save();

        return redirect('/dashboard/paginas');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        //
    }

    public function checkSlug($slug)
    {
        $slug = Product::where('slug', $slug)->count();

        if ($slug > 0) {
            return response()->json(true);
        }

        return response()->json(false);
    }

    public function getCodes($id)
    {
        $codes = Product::find($id)->codes;

        foreach ($codes as $code) {
            $code->lead;
        }

        return response()->json($codes);
    }

    public function showPage($slug)
    {
        $product = Product::where('slug',$slug)->where('status',1)->first();

        if(!$product){
            return redirect('/');
        }

        return view('product.page',[
            'product' => $product,
        ]);
    }

    public function showMainPage()
    {
        $product = Product::where('status',1)->where('main_product',1)->first();

        if(!$product){
            return view('default');
        }

        return view('product.page',[
            'product' => $product,
        ]);
    }

    public function delete($page_id)
    {
        $page = Product::find($page_id);

        if ($page->codes()->where('status',1)->count() == 0) {
            $page->delete();
        }

        return redirect('/dashboard/paginas');
    }
}
