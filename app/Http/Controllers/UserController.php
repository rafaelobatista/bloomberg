<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Auth::user()->role != 'admin') {
            return redirect('/dashboard');
        }

        $users = User::all();

        return view('dashboard.users.index',[
            'users' => $users,  
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (Auth::user()->role != 'admin') {
            return redirect('/dashboard');
        }

        return view('dashboard.users.form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (Auth::user()->role != 'admin') {
            return redirect('/dashboard');
        }

        $user = new User;
        $user->name = $request->input('name');
        $user->email = $request->input('email');
        $user->role = $request->input('role');
        $user->country = $request->input('country');

        if($request->input('password_confirmation') === $request->input('password')){
            $user->password = Hash::make($request->input('password'));
            $user->save();
        }

        return redirect('/dashboard/usuarios');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (Auth::user()->role != 'admin' && Auth::user()->id != $id) {
            return redirect('/dashboard/usuarios/editar/' . Auth::user()->id);
        }

        $user = User::find($id);

        if($user){
            return view('dashboard.users.edit',[
                'user' => $user,
            ]);
        }else{
            return redirect('/dashboard/usuarios');
        }

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $user = User::find($id);

        // Se o usuário autenticado for administrador ele pode alterar os dados dos outros usuários, mas nunca a senha
        if (Auth::user()->role == 'admin') {
            $user->name = $request->input('name');
            $user->email = $request->input('email');
            $user->role = $request->input('role');
            $user->country = $request->input('country');
        }

        // Se o usuário autenticado for o mesmo usuário da edição ele pode alterar a senha
        if($request->input('password_confirmation') === $request->input('password') && Auth::user()->id == $id){
            $user->password = Hash::make($request->input('password'));
        }

        $user->save();

        return redirect('/dashboard/usuarios');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
