<?php

namespace App\Http\Controllers;

use App\Models\Code;
use App\Models\Product;
use Illuminate\Http\Request;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;

class CodeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Code  $code
     * @return \Illuminate\Http\Response
     */
    public function show(Code $code)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Code  $code
     * @return \Illuminate\Http\Response
     */
    public function edit(Code $code)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Code  $code
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Code $code)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Code  $code
     * @return \Illuminate\Http\Response
     */
    public function destroy(Code $code)
    {
        //
    }

    public function validateCode($code,$product_id)
    {
        $code = Code::where('code',$code)->where('product_id', $product_id)->where('status',0)->first();
        
        if(!$code){
            return response()->json('invalid');            
        }

        $code->product;

        return response()->json($code);
    }

    public function codesToSpreadsheet($page_id)
    {

        if (Auth::user()->role != 'blmbrg') {
            return redirect('/dashboard');
        }


        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();

        $sheet->setCellValue('A1', 'CÓDIGO');
        $sheet->setCellValue('B1', 'NOME');
        $sheet->setCellValue('C1', 'EMPRESA');
        $sheet->setCellValue('D1', 'ENTREGA');

        $codes = Product::find($page_id)->codes()->get();
        $i = 2;

        foreach($codes as $code){            
            $sheet->setCellValue('A'.$i, $code->code);
            $sheet->setCellValue('B'.$i, isset($code->lead->name) ? $code->lead->name : '');
            $sheet->setCellValue('C'.$i, isset($code->lead->company) ? $code->lead->company : '');

            if(isset($code->lead->delivery_status) && $code->lead->delivery_status == 1){
                $delivery_status = 'Entregue';
            }else{
                $delivery_status = 'Pendente';
            }
            $sheet->setCellValue('D'.$i, $delivery_status);

            $i++;
        }

        $writer = new Xlsx($spreadsheet);
        $sheet_name = 'Códigos - '. $code->product->title .' - ' . env('APP_NAME') . ' ' . Carbon::now()->format('YmdHisu') . '.xlsx';
        $writer->save('downloads/'.$sheet_name);

        header('Content-Disposition: attachment; filename=' . $sheet_name );
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Length: ' . filesize('downloads/'.$sheet_name));
        header('Content-Transfer-Encoding: binary');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');
        readfile('downloads/'.$sheet_name); 
    }
}
