<?php

namespace App\Http\Controllers;

use App\Models\Lead;
use App\Models\Code;
use App\Models\Product;
use Illuminate\Http\Request;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;

class LeadController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Auth::user()->role != 'outsource') {
            return redirect('/dashboard');
        }

        $products = Product::where('country',Auth::user()->country)->get();

        return view('dashboard.leads.index', [
            'products' => $products,
        ]);
    }

    public function leadsByProduct($product_id)
    {
        if (Auth::user()->role != 'outsource') {
            return redirect('/dashboard');
        }

        $leads = Lead::where('product_id',$product_id)->get();

        return view('dashboard.leads.leads_by_products', [
            'leads' => $leads,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $code = Code::where('status',0)->where('product_id',$request->input('product_id'))->where('code',$request->input('used_code'))->first();

        if($code){
            $lead = new Lead;
            $lead->name = $request->input('name');
            $lead->lastname = $request->input('lastname');
            $lead->company = $request->input('company');
            $lead->email = $request->input('email');
            $lead->phone = $request->input('phone');
            $lead->country = $request->input('country');
            $lead->address = $request->input('address');
            $lead->address_complement = $request->input('address_complement');
            $lead->city = $request->input('city');
            $lead->state = $request->input('state');
            $lead->postal_code = $request->input('postal_code');
            $lead->promotional_code = $request->input('used_code');
            $lead->product_id = $code->product_id;
            $lead->save();

            $code->lead_id = $lead->id;
            $code->status = 1;
            $code->save();
            
            return response()->json(true);
        }else{
            return response()->json(false);

        }

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Lead  $lead
     * @return \Illuminate\Http\Response
     */
    public function show(Lead $lead)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Lead  $lead
     * @return \Illuminate\Http\Response
     */
    public function edit(Lead $lead)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Lead  $lead
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Lead $lead)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Lead  $lead
     * @return \Illuminate\Http\Response
     */
    public function destroy(Lead $lead)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Lead  $lead
     * @return \Illuminate\Http\Response
     */
    public function spreadsheet($product_id)
    {
        if (Auth::user()->role != 'outsource') {
            return redirect('/dashboard');
        }


        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();

        $sheet->setCellValue('A1', 'ID');
        $sheet->setCellValue('B1', 'NOME');
        $sheet->setCellValue('C1', 'SOBRENOME');
        $sheet->setCellValue('D1', 'EMPRESA');
        $sheet->setCellValue('E1', 'EMAIL');
        $sheet->setCellValue('F1', 'TELEFONE');
        $sheet->setCellValue('G1', 'PAÍS');
        $sheet->setCellValue('H1', 'ENDEREÇO');
        $sheet->setCellValue('I1', 'COMPLEMENTO');
        $sheet->setCellValue('J1', 'CIDADE');
        $sheet->setCellValue('K1', 'ESTADO');
        $sheet->setCellValue('L1', 'CEP');
        $sheet->setCellValue('M1', 'PRODUTO');
        $sheet->setCellValue('N1', 'ENTREGA');

        $leads = Lead::where('product_id',$product_id)->get();
        $i = 2;

        foreach($leads as $lead){
            $sheet->setCellValue('A'.$i, $lead->id);
            $sheet->setCellValue('B'.$i, $lead->name);
            $sheet->setCellValue('C'.$i, $lead->lastname);
            $sheet->setCellValue('D'.$i, $lead->company);
            $sheet->setCellValue('E'.$i, $lead->email);
            $sheet->setCellValue('F'.$i, $lead->phone);
            $sheet->setCellValue('G'.$i, $lead->country);
            $sheet->setCellValue('H'.$i, $lead->address);
            $sheet->setCellValue('I'.$i, $lead->address_complement);
            $sheet->setCellValue('J'.$i, $lead->city);
            $sheet->setCellValue('K'.$i, $lead->state);
            $sheet->setCellValue('L'.$i, $lead->postal_code);
            $sheet->setCellValue('M'.$i, $lead->product->title);
            $sheet->setCellValue('N'.$i, $lead->delivery_status == 1 ? 'Entregue' : 'Pendente');

            $i++;
        }

        $writer = new Xlsx($spreadsheet);
        $sheet_name = 'Leads - ' . env('APP_NAME') . ' ' . Carbon::now()->format('YmdHisu') . '.xlsx';
        $writer->save('downloads/'.$sheet_name);

        header('Content-Disposition: attachment; filename=' . $sheet_name );
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Length: ' . filesize('downloads/'.$sheet_name));
        header('Content-Transfer-Encoding: binary');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');
        readfile('downloads/'.$sheet_name); 

    }

    public function updateDeliveryStatus($lead_id, $status)
    {
        $lead = Lead::find($lead_id);
        $lead->delivery_status = $status;
        $lead->save();

        return $lead;
    }
}
