<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;

    public function Leads()
    {
        return $this->hasMany('App\Models\Lead');
    }

    public function Codes()
    {
        return $this->hasMany('App\Models\Code')->orderBy('status');
    }
}
